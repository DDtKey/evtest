CREATE TABLE public.ev_user (
    username VARCHAR,
    password VARCHAR,
    balance  NUMERIC,
    CONSTRAINT pk_user PRIMARY KEY (username)
);