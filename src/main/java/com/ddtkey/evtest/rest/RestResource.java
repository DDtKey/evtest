package com.ddtkey.evtest.rest;

import com.ddtkey.evtest.service.UserService;
import com.ddtkey.evtest.service.dto.ClientRequestDTO;
import com.ddtkey.evtest.service.dto.ClientResponseDTO;
import org.glassfish.jersey.server.ManagedAsync;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.concurrent.TimeUnit;

@Path("/api")
public class RestResource {
    private final static Logger LOG = LoggerFactory.getLogger(RestResource.class);

    private final UserService userService;

    public RestResource(UserService userService) {
        this.userService = userService;
    }

    @POST
    @Path("clients")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ManagedAsync
    public void clientRequest(@Suspended final AsyncResponse asyncResponse, ClientRequestDTO requestDTO) {
        LOG.info("Client request: {}", requestDTO.toString());
        setTimeout(asyncResponse);

        ClientResponseDTO responseDTO = getClientResponseByType(requestDTO);
        asyncResponse.resume(Response
                .status(responseDTO.getResult().getHttpCode())
                .entity(responseDTO).build());
    }

    private ClientResponseDTO getClientResponseByType(ClientRequestDTO requestDTO) {
        switch (requestDTO.getType()) {
            case CREATE: {
                return this.userService.createUser(requestDTO);
            }
            case GET_BALANCE: {
                return this.userService.getBalance(requestDTO);
            }
            default: {
                throw new RuntimeException("Unexpected request-type");
            }
        }
    }

    private void setTimeout(AsyncResponse asyncResponse) {
        asyncResponse.setTimeout(5000, TimeUnit.MILLISECONDS);
        asyncResponse.setTimeoutHandler(ar -> ar.resume(
                        Response.status(Response.Status.SERVICE_UNAVAILABLE)
                                .entity(new ClientResponseDTO(ClientResponseDTO.ResultCode.SERVER_ERROR))
                                .build()
        ));
    }

}
