package com.ddtkey.evtest.rest;

import com.ddtkey.evtest.service.dto.ClientResponseDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by Artem Medvedev.
 * Date: 31.01.2019
 */
@Provider
public class EvtestExceptionMapper implements ExceptionMapper<Exception> {
    private final static Logger LOG = LoggerFactory.getLogger(EvtestExceptionMapper.class);

    @Override
    public Response toResponse(Exception exception) {
        LOG.error(exception.getMessage(), exception);
        return Response.status(500).entity(new ClientResponseDTO(ClientResponseDTO.ResultCode.SERVER_ERROR)).build();
    }
}
