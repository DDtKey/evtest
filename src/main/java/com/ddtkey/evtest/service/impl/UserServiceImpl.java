package com.ddtkey.evtest.service.impl;

import com.ddtkey.evtest.entity.User;
import com.ddtkey.evtest.repository.UserRepository;
import com.ddtkey.evtest.service.UserService;
import com.ddtkey.evtest.service.dto.ClientRequestDTO;
import com.ddtkey.evtest.service.dto.ClientResponseDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

public class UserServiceImpl implements UserService {
    private final static Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public ClientResponseDTO createUser(ClientRequestDTO requestDTO) {
        try {
            if (userRepository.exists(requestDTO.getLogin())) {
                return new ClientResponseDTO(ClientResponseDTO.ResultCode.ALREADY_EXISTS_USER);
            }
            User newUser = new User(
                    requestDTO.getLogin(),
                    requestDTO.getPassword(),
                    BigDecimal.ZERO
            );
            userRepository.add(newUser);
            return new ClientResponseDTO(ClientResponseDTO.ResultCode.OK);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return new ClientResponseDTO(ClientResponseDTO.ResultCode.SERVER_ERROR);
        }
    }

    @Override
    public ClientResponseDTO getBalance(ClientRequestDTO requestDTO) {
        try {
            if (!userRepository.exists(requestDTO.getLogin())) {
                return new ClientResponseDTO(ClientResponseDTO.ResultCode.USER_NOT_EXIST);
            }
            User user = userRepository.findOne(requestDTO.getLogin());

            if (!user.getPassword().equals(requestDTO.getPassword())) {
                return new ClientResponseDTO(ClientResponseDTO.ResultCode.PASSWORD_INCORRECT);
            }

            ClientResponseDTO responseDTO = new ClientResponseDTO(ClientResponseDTO.ResultCode.OK);
            responseDTO.addToExtras("balance", user.getBalance());

            return responseDTO;

        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return new ClientResponseDTO(ClientResponseDTO.ResultCode.SERVER_ERROR);
        }
    }

}
