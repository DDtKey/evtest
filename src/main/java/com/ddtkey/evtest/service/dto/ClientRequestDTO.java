package com.ddtkey.evtest.service.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Arrays;
import java.util.Objects;

public class ClientRequestDTO {

    public enum Type {
        CREATE("create"), GET_BALANCE("get-balance");

        private String value;

        Type(String value) {
            this.value = value;
        }

        @JsonCreator
        public static Type forValue(String value) {
            return Arrays.stream(Type.values())
                    .filter(e -> e.value.equals(value))
                    .findFirst()
                    .orElse(null);
        }

        @JsonValue
        public String getValue() {
            return value;
        }
    }

    private Type type;

    private String login;

    private String password;

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientRequestDTO that = (ClientRequestDTO) o;
        return type == that.type &&
                Objects.equals(login, that.login) &&
                Objects.equals(password, that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, login, password);
    }

    @Override
    public String toString() {
        return "ClientRequestDTO{" +
                "type='" + type + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
