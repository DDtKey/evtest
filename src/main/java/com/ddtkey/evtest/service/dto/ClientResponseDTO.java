package com.ddtkey.evtest.service.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonValue;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class ClientResponseDTO {

    public enum ResultCode {
        OK(0, 200),
        ALREADY_EXISTS_USER(1, 400),
        SERVER_ERROR(2, 500),
        USER_NOT_EXIST(3, 404),
        PASSWORD_INCORRECT(4, 400);

        private int code;
        private int httpCode;

        ResultCode(int code, int httpCode) {
            this.code = code;
            this.httpCode = httpCode;
        }

        @JsonCreator
        public static ResultCode forCode(int code) {
            return Arrays.stream(ResultCode.values())
                    .filter(e -> e.code == code)
                    .findFirst()
                    .orElse(null);
        }

        @JsonValue
        public int getCode() {
            return code;
        }

        public int getHttpCode() {
            return httpCode;
        }
    }

    private ResultCode result;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Map<String, BigDecimal> extras = new HashMap<>();

    public ClientResponseDTO(ResultCode result) {
        this.result = result;
    }

    public ClientResponseDTO() {
    }

    public ResultCode getResult() {
        return result;
    }

    public void setResult(ResultCode result) {
        this.result = result;
    }

    public Map<String, BigDecimal> getExtras() {
        return extras;
    }

    public void setExtras(Map<String, BigDecimal> extras) {
        this.extras = extras;
    }

    public void addToExtras(String code, BigDecimal value) {
        this.extras.put(code, value);
    }

    public BigDecimal getExtras(String code) {
        return this.extras.get(code);
    }

    @Override
    public String toString() {
        return "ClientResponseDTO{" +
                "result=" + result +
                ", extras=" + extras +
                '}';
    }
}
