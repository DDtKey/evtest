package com.ddtkey.evtest.service;

import com.ddtkey.evtest.service.dto.ClientRequestDTO;
import com.ddtkey.evtest.service.dto.ClientResponseDTO;

public interface UserService {

    ClientResponseDTO createUser(ClientRequestDTO requestDTO);
    ClientResponseDTO getBalance(ClientRequestDTO requestDTO);
}
