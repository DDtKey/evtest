package com.ddtkey.evtest.repository;

import com.ddtkey.evtest.entity.User;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserRepository implements Repository<User, String> {

    private final static String CREATE_QUERY = "insert into ev_user(username, password, balance) values (?,?,?)";
    private final static String UPDATE_QUERY = "update ev_user set password = ?, balance = ? where username = ?";
    private final static String DELETE_QUERY = "delete from ev_user where username = ?";
    private final static String GET_BY_ID_QUERY = "select * from ev_user where username = ?";
    private final static String EXISTS_USER_QUERY =
            "select case when exists " +
                "(select * from ev_user where username = ?)" +
            "then true " +
            "else false end";

    private final DataSource dataSource;

    public UserRepository(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public User add(User item) throws SQLException {
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(CREATE_QUERY)) {
                statement.setString(1, item.getUsername());
                statement.setString(2, item.getPassword());
                statement.setBigDecimal(3, item.getBalance());
                statement.executeUpdate();
            }
        }
        return findOne(item.getUsername());
    }

    @Override
    public User update(User item) throws SQLException {
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(UPDATE_QUERY)) {
                statement.setString(1, item.getPassword());
                statement.setBigDecimal(2, item.getBalance());
                statement.setString(3, item.getUsername());
                statement.executeUpdate();
            }
        }
        return findOne(item.getUsername());
    }

    @Override
    public void remove(String id) throws SQLException {
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(DELETE_QUERY)) {
                statement.setString(1, id);
                statement.execute();
            }
        }
    }

    @Override
    public boolean exists(String id) throws SQLException {
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(EXISTS_USER_QUERY)) {
                statement.setString(1, id);
                try (ResultSet resultSet = statement.executeQuery()) {
                    resultSet.next();
                    return resultSet.getBoolean(1);
                }
            }
        }
    }

    @Override
    public User findOne(String username) throws SQLException {
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(GET_BY_ID_QUERY)) {
                statement.setString(1, username);
                try (ResultSet resultSet = statement.executeQuery()) {
                    resultSet.next();
                    return convertResultSetToUser(resultSet);
                }
            }
        }
    }

    private User convertResultSetToUser(ResultSet resultSet) throws SQLException {
        String username = resultSet.getString("username");
        String password = resultSet.getString("password");
        BigDecimal balance = resultSet.getBigDecimal("balance");
        return new User(username, password, balance);
    }
}
