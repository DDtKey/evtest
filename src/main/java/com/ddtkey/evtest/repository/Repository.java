package com.ddtkey.evtest.repository;

import java.sql.SQLException;

public interface Repository<T, ID> {

    T add(T item) throws SQLException;
    T update(T item) throws SQLException;
    void remove(ID id) throws SQLException;
    boolean exists(ID id) throws SQLException;
    T findOne(ID id) throws SQLException;
}
