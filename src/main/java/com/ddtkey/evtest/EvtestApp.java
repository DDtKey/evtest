package com.ddtkey.evtest;

import com.ddtkey.evtest.repository.UserRepository;
import com.ddtkey.evtest.rest.EvtestExceptionMapper;
import com.ddtkey.evtest.rest.RestResource;
import com.ddtkey.evtest.service.UserService;
import com.ddtkey.evtest.service.impl.UserServiceImpl;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.flywaydb.core.Flyway;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class EvtestApp {
    private final static Logger LOG = LoggerFactory.getLogger(EvtestApp.class);

    private final static Map<Class, Object> APP_CONTEXT = new ConcurrentHashMap<>();

    private final static String DATA_SOURCE_PROP_FILENAME = "src/main/resources/datasource.properties";

    public static void main(String[] args) {
        try {
            initDataSource();
            initMigrationScripts();
            initRepositories();
            initServices();
            initRestApi();
        } catch (Exception e) {
            LOG.error("Error starting or running the server.", e);
        }
    }

    private static void initRepositories() {
        final var repository = new UserRepository(getObjectFromContext(DataSource.class));
        APP_CONTEXT.put(UserRepository.class, repository);
    }

    private static void initServices() {
        final var service = new UserServiceImpl(getObjectFromContext(UserRepository.class));
        APP_CONTEXT.put(UserService.class, service);
    }

    private static void initDataSource() {
        var config = new HikariConfig(DATA_SOURCE_PROP_FILENAME);
        var dataSource = new HikariDataSource(config);
        APP_CONTEXT.put(DataSource.class, dataSource);
    }

    private static void initMigrationScripts() {
        var flyway = Flyway.configure()
                .dataSource(getObjectFromContext(DataSource.class))
                .load();
        flyway.migrate();
    }

    private static void initRestApi() {
        var contextHandler = new ServletContextHandler(ServletContextHandler.SESSIONS);
        contextHandler.setContextPath("/");

        var server = new Server(8080);
        server.setHandler(contextHandler);

        var servletConfig = new ResourceConfig()
                .register(new RestResource(getObjectFromContext(UserService.class)))
                .register(new EvtestExceptionMapper());

        var servletContainer = new ServletContainer(servletConfig);
        var jerseyHolder = new ServletHolder(servletContainer);

        contextHandler.addServlet(jerseyHolder, "/*");

        try {
            server.start();
            server.join();
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        } finally {
            server.destroy();
        }
    }

    /**
     * @return thread safe application context
     */
    public static Map<Class, Object> getAppContext() {
        return APP_CONTEXT;
    }

    /**
     * @param key - class of the required object.
     * @param <T> - casted type.
     * @return casted object from context.
     */
    public static <T> T getObjectFromContext(Class<T> key) {
        return key.cast(APP_CONTEXT.get(key));
    }


}
